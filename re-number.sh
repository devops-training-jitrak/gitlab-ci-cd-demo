#!/bin/bash

sed -i "s|\"00\"|\"$1\"|g" kubernetes-template/staging/*
sed -i "s|-00|-$1|g" kubernetes-template/staging/*

sed -i "s|\"00\"|\"$1\"|g" kubernetes-template/production/*
sed -i "s|-00|-$1|g" kubernetes-template/production/*